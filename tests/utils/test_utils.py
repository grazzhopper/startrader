import os.path
from pathlib import PosixPath

from logic.utils.utils import get_project_dir


def test_get_project_directory():
    """Tests if the project directory returned is correct and exists."""
    assert get_project_dir() == PosixPath('/home/grazzhopper/dev/startrader')
    assert os.path.exists(get_project_dir()) == 1
