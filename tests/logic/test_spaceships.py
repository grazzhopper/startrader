import pytest

from logic.cargo.cargoitem import CargoItem, Product, ProductCategory
from logic.spaceships.spaceship import Spaceship, Freighter


def test_spaceship():
    """ Test if all parameters are set correctly. """
    ship = Spaceship()
    assert ship.velocity == 1.0
    assert ship.heading.initial_point == (0, 0, 0)
    assert ship.heading.terminal_point == (0, 0, 0)
    assert ship.current_location == (0, 0, 0)
    assert ship.heading.scalar == 1.0
    assert ship.name == "Indignation"
    assert ship.max_fuel == 1000
    assert ship.fuel == 1000


def test_freighter():
    """ Test if a freighter is initialized properly. """
    ship = Freighter()
    assert ship.velocity == 0.5
    assert ship.heading.initial_point == (0, 0, 0)
    assert ship.heading.terminal_point == (0, 0, 0)
    assert ship.heading.scalar == 1.0
    assert ship.cargo_capacity == 50
    assert ship.cargo is None
    assert ship.name == "Indignation"
    assert ship.max_fuel == 1500
    assert ship.fuel == 1500


def test_loading_freighter():
    """ Test when loading a freighter that it isn't overloaded and the cargo
    capacity is sufficient. """

    # load a freighter with too much cargo on the list.
    ship = Freighter()
    cargo = [CargoItem(product=Product(description="furniture",
                                       category=ProductCategory.CONTAINERS),
                       quantity=10, buy_value=100, ),
             CargoItem(product=Product(description="grain",
                                       category=ProductCategory.BULK),
                       quantity=20, buy_value=100, ),
             CargoItem(product=Product(description="Beer",
                                       category=ProductCategory.LIQUIDS),
                       quantity=30, buy_value=100, )]
    with pytest.raises(ValueError, match=r"Too much cargo to load"):
        ship.load_cargo(cargo)

    # load an already loaded ship with too much cargo
    with pytest.raises(ValueError, match="Too much cargo to load"):
        hold = [CargoItem(product=Product(description="furniture",
                                          category=ProductCategory.CONTAINERS),
                          quantity=1000, buy_value=100, ),
                CargoItem(product=Product(description="grain",
                                          category=ProductCategory.BULK),
                          quantity=20, buy_value=100, ),
                CargoItem(product=Product(description="Beer",
                                          category=ProductCategory.LIQUIDS),
                          quantity=30, buy_value=100, )]
        ship = Freighter(cargo=hold)

        load = [CargoItem(product=Product(description="furniture",
                                          category=ProductCategory.CONTAINERS),
                          quantity=10, buy_value=100, )]
        ship.load_cargo(load)

    # load a freighter with cargo within capacity bounds

    hold = [CargoItem(product=Product(description="furniture",
                                      category=ProductCategory.CONTAINERS),
                      quantity=10, buy_value=100, ),
            CargoItem(product=Product(description="grain",
                                      category=ProductCategory.BULK),
                      quantity=10, buy_value=100, )]

    ship = Freighter(cargo=hold)

    load = [CargoItem(product=Product(description="furniture",
                                      category=ProductCategory.CONTAINERS),
                      quantity=10, buy_value=100, ),
            CargoItem(product=Product(description="beer",
                                      category=ProductCategory.LIQUIDS),
                      quantity=10, buy_value=100, )]
    res = ship.load_cargo(load)
    assert res == (20, 40)
    assert ship.cargo == \
           [CargoItem(product=Product(description="furniture",
                                      category=ProductCategory.CONTAINERS, ),
                      quantity=20, buy_value=200, ),
            CargoItem(product=Product(description="grain",
                                      category=ProductCategory.BULK),
                      quantity=10, buy_value=100, ),
            CargoItem(product=Product(description="beer",
                                      category=ProductCategory.LIQUIDS),
                      quantity=10, buy_value=100, )]
