from logic.cargo.cargoitem import Product, ProductCategory, CargoItem


def test_cargo():
    """ Test if all parameters are set correctly. """
    item1 = CargoItem(product=Product(description="furniture",
                                      category=ProductCategory.CONTAINERS),
                      quantity=10, buy_value=100, )
    item2 = CargoItem(product=Product(description="grain",
                                      category=ProductCategory.BULK),
                      quantity=20, buy_value=100, )
    item3 = CargoItem(product=Product(description="Beer",
                                      category=ProductCategory.LIQUIDS),
                      quantity=20, buy_value=100, )
    cargo = [item1, item2, item3]
    assert cargo[0].product.category.value == "containers"
    assert cargo[1].product.category.value == "bulk"
    assert cargo[2].product.category.value == "liquids"
    assert cargo[0].quantity == 10
    assert cargo[1].quantity == 20
    assert cargo[2].quantity == 20
