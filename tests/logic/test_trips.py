from math import sqrt

from logic.trips.legs import Leg
from logic.trips.trip import Trip
from logic.vectors.vector3d import Vector3d


def test_trips():
    trip = Trip(velocity=0.5, legs=[Leg(route=Vector3d(initial_point=(0, 0, 0),
                                                       terminal_point=(
                                                           250, 250, 250)))])
    assert trip.legs[0].route.initial_point == (0, 0, 0)
    assert trip.legs[0].route.terminal_point == (250, 250, 250)
    assert trip.legs[0].route.get_magnitude3d() == sqrt(3 * (250 ** 2))
    # TODO: need to add travel-time to the trip per leg.
