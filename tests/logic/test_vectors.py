from math import sqrt

from logic.vectors.vector2d import Vector2d
from logic.vectors.vector3d import Vector3d


def test_vector2d():
    """ This function tests if all parameters are getting set correctly. """
    v = Vector2d((-4, 2), (4, 5), scalar=2)
    assert v.type == "vector"
    assert v.initial_point == (-4, 2)
    assert v.terminal_point == (4, 5)
    assert v.scalar == 2


def test_get_positional_vector2d():
    pv = Vector2d((-4, 2), (4, 5), scalar=2).get_position_vector2d()
    assert pv.type == "position vector"
    assert pv.initial_point == (0, 0)
    assert pv.terminal_point == (8, 3)
    assert pv.scalar == 2


def test_get_unit_vector2d():
    iv = Vector2d((-2, -2), (10, 4), scalar=3)
    uv = iv.get_unit_vector2d()
    assert uv.type == "unit vector"
    assert uv.initial_point == (0, 0)
    assert uv.terminal_point == (4, 2)
    assert uv.get_magnitude2d() == iv.get_magnitude2d() / iv.scalar
    assert uv.scalar == 1


def test_get_magnitude2d():
    v = Vector2d((5, -3), (-1, 2), scalar=2)
    assert v.get_magnitude2d() == sqrt(61)
    assert v.get_magnitude2d() == v.get_position_vector2d().get_magnitude2d()


def test_vector3d():
    v = Vector3d((3, 1, 2), (6, -2, 5), scalar=2)
    assert v.type == "vector"
    assert v.initial_point == (3, 1, 2)
    assert v.terminal_point == (6, -2, 5)
    assert v.scalar == 2


def test_get_position_vector3d():
    v = Vector3d((3, 1, 2), (6, -2, 5), scalar=2).get_position_vector3d()
    assert v.type == "position vector"
    assert v.initial_point == (0, 0, 0)
    assert v.terminal_point == (3, -3, 3)
    assert v.scalar == 2


def test_get_unit_vector3d():
    iv = Vector3d((3, 1, 2), (6, -2, 5), scalar=2)
    uv = iv.get_unit_vector3d()
    assert uv.type == "unit vector"
    assert uv.initial_point == (0, 0, 0)
    assert uv.terminal_point == (1.5, -1.5, 1.5)
    assert uv.get_magnitude3d() == iv.get_magnitude3d() / iv.scalar
    assert uv.scalar == 1
