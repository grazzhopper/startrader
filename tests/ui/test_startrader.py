from typer.testing import CliRunner

from startrader import app

runner = CliRunner()


def test_show_cargo():
    result = runner.invoke(app, ["show", "cargo"])
    assert "Showing cargo" in result.stdout
    assert result.exit_code == 0


def test_show_ship():
    result = runner.invoke(app, ["show", "ship"])
    assert "Showing ship" in result.stdout
    assert result.exit_code == 0


def test_show_port():
    result = runner.invoke(app, ["show", "port"])
    assert "Showing port" in result.stdout
    assert result.exit_code == 0
