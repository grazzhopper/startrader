#!/usr/bin/python

import typer

from ui import show, load

app = typer.Typer()

app.add_typer(show.app, name="show")
app.add_typer(load.app, name="load")

if __name__ == "__main__":
    app()
