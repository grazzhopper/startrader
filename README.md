# Startrader

Startraders is an attempt to create a command-line trading game set in space.
The game will take commands to steer your freighter ship, the "Indignation" to stars and riches.

The game is written in Python and uses Typer as a command-line utility and Rich as the formatting library to make the
application look good.
I tried to keep the dependencies on other projects and libraries as little as possible. The vector based travel is based
on my own calculation library.

## How to install and run

Check if you have at least Python v3.10 installed and clone the repo into your home directory at a desired location.
The startraders.py python script is made executable and can be run as shown underneath:

```commandline
git clone https://gitlab.com/grazzhopper/startrader.git 
cd startrader/src
chmod +x startrader.py
./startrader.py --help
```

You can set a path to this script and an alias to startraders.py to make it behave like a normal command line app you
are used to with:

```commandline
export PATH="~/startraders/src:$PATH"
alias startraders="startraders.py"
```

## Contributing

Startrader is a free and open source project. If you want to contribute you can open an issue or pull request on this
project.