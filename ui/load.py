import typer

app = typer.Typer()


@app.command()
def ship():
    typer.echo("Loading ship.")


@app.command()
def fuel():
    typer.echo("Loading fuel.")


if __name__ == "__main__":
    app()
