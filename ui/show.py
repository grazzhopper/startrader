import json
import os

import typer
from rich import box
from rich.console import Console
from rich.table import Table

from logic.utils.utils import get_project_dir

app = typer.Typer()
console = Console()


def load_ships(nickname: str):
    file = open(os.path.join(get_project_dir(), "database/ships.json"))
    data = json.load(file)
    for s in data:
        if s['nickname'] == nickname:
            return s


@app.command()
def ship():
    ship_data = load_ships(nickname="The box")
    table = Table(title="Showing ship", show_header=True,
                  header_style="bold magenta", box=box.ROUNDED)
    table.add_column("Nickname", style="bold", width=12)
    table.add_column("Manufacturer")
    table.add_column("Type")
    table.add_column("Classification")
    table.add_column("Max. cargo", justify="right")
    table.add_column("Description", style="dim", width=50)
    table.add_row(
        ship_data["nickname"], ship_data["manufacturer"], ship_data["type"],
        ship_data["classification"],
        str(ship_data["specifications"]["cargo_capacity"]),
        ship_data["description"]
    )
    console.print(table)


@app.command()
def cargo():
    typer.echo("Showing cargo.")


@app.command()
def port():
    typer.echo("Showing port.")


if __name__ == "__main__":
    app()
