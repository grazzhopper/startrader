from dataclasses import dataclass
from enum import Enum


class ProductCategory(Enum):
    LIQUIDS = "liquids"
    BULK = "bulk"
    CONTAINERS = "containers"


@dataclass()
class Product:
    description: str
    category: ProductCategory


@dataclass()
class CargoItem:
    """This class holds the cargo items of Products."""
    quantity: int
    product: Product
    buy_value: int
