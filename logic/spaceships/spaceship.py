from logic.cargo.cargoitem import CargoItem
from logic.vectors.vector3d import Vector3d


class Spaceship:
    """ This class represents a basic spaceship. """

    def __init__(self, heading: Vector3d = Vector3d((0, 0, 0), (0, 0, 0)),
                 current_location: tuple = (0, 0, 0),
                 velocity: float = 1.0,
                 name: str = "Indignation", max_fuel: int = 1000,
                 fuel: int = 1000, ) -> None:
        """
        This method constructs a Vector2d object that represents a basic
        vector.

        :param current_location: tuple that represents a position point.
        :param max_fuel: the maximum amount of fuel the ship can take.
        :param fuel: the current fuel on board.
        :param velocity: represents the time it takes to travel through one
        unit of space.
        :param heading: the heading is represented by a (unit)vector.
        :param name: The name of the ship.
        :returns: None
        """

        self.velocity = velocity
        self.current_location = current_location
        self.heading = heading
        self.name = name
        self.max_fuel = max_fuel
        self.fuel = fuel


class Freighter(Spaceship):
    """ This class represents a type of spaceship called a freighter. A
    freighter is used to travel long distances and haul massive amounts of
    goods.
    """

    def __init__(self, heading: Vector3d = Vector3d((0, 0, 0), (0, 0, 0)),
                 velocity: float = 0.5,
                 name: str = "Indignation",
                 current_location: tuple = (0, 0, 0),
                 max_fuel: int = 1500, fuel: int = 1500,
                 cargo_capacity: int = 50,
                 cargo: list[CargoItem] = None) -> None:
        super().__init__(heading, current_location, velocity, name, max_fuel,
                         fuel)
        self.cargo_capacity = cargo_capacity
        self.cargo = cargo

    def load_cargo(self, cargo: list[CargoItem]) -> tuple[int, int]:
        """ Load cargo and check if the spaceship doesn't overload.

        :param cargo: the list of cargo items to be loaded.
        :returns: a tuple of (cargo_loaded,  total_cargo)
        after loading.
        :raises ValueError: if the ships overloads above maximum capacity.
        """

        # quantity that needs to be loaded.
        load_quantity = sum([i.quantity for i in cargo])

        # calculate the current cargo quantity in the ship.
        if self.cargo is not None:
            cargo_holds = sum(i.quantity for i in self.cargo)
        else:
            cargo_holds = 0

        # check if the cargo capacity is sufficient.
        if load_quantity <= self.cargo_capacity - cargo_holds:
            cargo_loaded = load_quantity

            # if there is no cargo define it as an empty list.
            if self.cargo is None:
                self.cargo = []

            # add the cargo to the list or to the same product if it was
            # already loaded.
            for n in range(len(cargo)):
                if self.cargo[n].product == cargo[n].product:
                    self.cargo[n].quantity += cargo[n].quantity
                    self.cargo[n].buy_value += cargo[n].buy_value
                else:
                    self.cargo.append(CargoItem(quantity=cargo[n].quantity,
                                                product=cargo[n].product,
                                                buy_value=cargo[n].buy_value))
        else:
            raise ValueError("Too much cargo to load")

        return cargo_loaded, sum(i.quantity for i in self.cargo)
