from math import sqrt


class Vector3d:
    def __init__(self,
                 initial_point: tuple[int | float, int | float, int | float],
                 terminal_point: tuple[int | float, int | float, int | float],
                 scalar: int = 1):
        self.initial_point = initial_point
        self.terminal_point = terminal_point
        self.scalar = scalar
        if self.initial_point == (0, 0, 0) and self.scalar != 1:
            self.type = "position vector"
        elif self.initial_point == (0, 0, 0) and self.scalar == 1:
            self.type = "unit vector"
        else:
            self.type = "vector"

    def get_position_vector3d(self):
        dx = self.terminal_point[0] - self.initial_point[0]
        dy = self.terminal_point[1] - self.initial_point[1]
        dz = self.terminal_point[2] - self.initial_point[2]
        return Vector3d((0, 0, 0), (dx, dy, dz), self.scalar)

    def get_unit_vector3d(self):
        pv = self.get_position_vector3d()
        return Vector3d((0, 0, 0),
                        (pv.terminal_point[0] / pv.scalar,
                         pv.terminal_point[1] / pv.scalar,
                         pv.terminal_point[2] / pv.scalar),
                        scalar=1)

    def get_magnitude3d(self) -> float:
        pv = self.get_position_vector3d()
        return sqrt(
            (pv.terminal_point[0] ** 2) + (pv.terminal_point[1] ** 2) + (
                    pv.terminal_point[2] ** 2))
