from math import sqrt


class Vector2d:
    """
    Class for working with 2d vectors.
    """

    def __init__(self, initial_point: tuple[int | float, int | float],
                 terminal_point: tuple[int | float, int | float],
                 scalar: int = 1) -> None:
        """
        This method constructs a Vector2d object that represents a basic
        vector.

        :param initial_point: the starting point of the vector
        :param terminal_point: the endpoint of the vector
        :param scalar: the quantity of the vector
        :returns: None
        """

        self.initial_point = initial_point
        self.terminal_point = terminal_point
        self.scalar = scalar
        if self.initial_point == (0, 0) and self.scalar != 1:
            self.type = "position vector"
        elif self.initial_point == (0, 0) and self.scalar == 1:
            self.type = "unit vector"
        else:
            self.type = "vector"

    def get_position_vector2d(self):
        """
        This method converts the Vector in a position vector with an initial
        point of (0,0) with the original scalar.

        :return: a Vector2d object
        """
        dx = self.terminal_point[0] - self.initial_point[0]
        dy = self.terminal_point[1] - self.initial_point[1]
        return Vector2d((0, 0), (dx, dy), self.scalar)

    def get_magnitude2d(self) -> float:
        """
        This method returns the length/magnitude of the vector object as a
        float.

        :return: the length/magnitude of the vector object.
        """
        pv = self.get_position_vector2d()
        return sqrt((pv.terminal_point[0] ** 2) + (pv.terminal_point[1] ** 2))

    def get_unit_vector2d(self):
        """
        This method takes no parameters and returns a Vector2d object of the
        vector object and recalculates the vector with the initial point at
        (0,0) and a scalar of 1.

        :return: a Vector2d object
        """
        pv = self.get_position_vector2d()
        return Vector2d((0, 0),
                        (pv.terminal_point[0] / pv.scalar,
                         pv.terminal_point[1] / pv.scalar),
                        scalar=1)
