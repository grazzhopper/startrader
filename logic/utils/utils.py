from pathlib import PosixPath


def get_project_dir() -> PosixPath:
    """This function returns an absolute path with the project root directory.

    :returns: PosixPath: project root directory
    """
    return PosixPath(__file__).parent.parent.parent
