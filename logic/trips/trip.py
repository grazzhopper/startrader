from logic.trips.legs import Leg


class Trip:
    def __init__(self, velocity: float, legs: list[Leg] = None, ):
        self.velocity = velocity
        self.legs = legs
