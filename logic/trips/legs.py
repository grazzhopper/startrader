from datetime import datetime

from logic.vectors.vector3d import Vector3d


class Leg:
    def __init__(self, route=Vector3d,
                 etd: datetime = None, eta: datetime = None,
                 atd: datetime = None, ata: datetime = None):
        self.route = route
        self.etd = etd
        self.eta = eta
        self.atd = atd
        self.ata = ata
